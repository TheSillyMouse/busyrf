#include "clock_hal.h"
#include "clock_drv.h"


void hal_clock_init(void)
{
	drv_clock_event_clear_hf_clk();
	drv_clock_task_set_hf_clk();

	while (drv_clock_event_get_hf_clk() == false);

	drv_clock_set_source(CLK_SRC_XTAL);
	drv_clock_event_clear_lf_clk();
	drv_clock_task_set_lf_clk();

	while (drv_clock_event_get_lf_clk() == false);	
}