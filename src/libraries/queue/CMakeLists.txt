add_library(queue OBJECT)

target_sources(queue PRIVATE queue.h queue.c)

target_include_directories(queue PUBLIC .)