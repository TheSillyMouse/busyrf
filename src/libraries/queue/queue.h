#ifndef QUEUE_H
#define QUEUE_H

#include <inttypes.h>
#include <stdbool.h>

typedef uint16_t QueueDataType_t;

struct queue
{
	QueueDataType_t * data;
	QueueDataType_t front_idx;
	QueueDataType_t back_idx;
	const QueueDataType_t size;
	QueueDataType_t elements;
};

void queue_init(struct queue * queue);

bool queue_is_full(struct queue * queue);

bool queue_is_empty(struct queue * queue);

bool queue_add_front(struct queue * queue, QueueDataType_t data);

bool queue_add_back(struct queue * queue, QueueDataType_t data);

bool queue_get_front(struct queue * queue, QueueDataType_t * data);

bool queue_get_back(struct queue * queue, QueueDataType_t * data);

#endif