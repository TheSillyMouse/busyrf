/**
 * \file    queue.c
 *
 * \brief   A double-ended queue (deque). Elements can be added or removed from
 *          either the front or the back side.
 * \warning The current implementation is NOT interrupt safe. Make sure interrupts
 *          are disabled before access the QUEUE otherwise the program might yield
 *          unexpected results.
*/

#include "queue.h"


#define INCREASE_INDEX(queue, idx)		queue->idx = ++queue->idx % queue->size
#define DECREASE_INDEX(queue, idx)		queue->idx = (--queue->idx + queue->size) % queue->size


/**
 * Initializes - resets the queue.
*/
void queue_init(struct queue * queue)
{
	memset(queue->data, 0, queue->size);
	queue->back_idx = 0;
	queue->front_idx = 0;
	queue->elements = 0;
}

/**
 * Checks if queue is full.
 * 
 * \returns true if queue is full.
*/
bool queue_is_full(struct queue * queue)
{
	return (queue->elements == queue->size);
}

/**
 * Checks if queue is empty
 * 
 * \returns true if queue is empty.
*/
bool queue_is_empty(struct queue * queue)
{
	return (queue->elements == 0);
}

/**
 * Adds one element to the front of the queue. 
 * 
 * \returns false if the queue is full. 
*/
bool queue_add_front(struct queue * queue, 
                     QueueDataType_t data)
{
	if (queue_is_full(queue))
	{
		return false;
	}

	if (queue_is_empty(queue) == false)
	{
		INCREASE_INDEX(queue, front_idx);
	}

	queue->data[queue->front_idx] = data;    
	queue->elements++;
	return true;
}

/**
 * Adds one element to the back of the queue.
 * 
 * \returns false if the queue is full. 
*/
bool queue_add_back(struct queue * queue, 
                    QueueDataType_t data)
{
	if (queue_is_full(queue))
	{
		return false;
	}

	if (queue_is_empty(queue) == false)
	{
		DECREASE_INDEX(queue, back_idx);
	}

	queue->data[queue->back_idx] = data;
	queue->elements++;
	return true;    
}

/**
 * Reads one element from the front of the queue.
 * 
 * \returns false if the queue is empty.
*/
bool queue_get_front(struct queue * queue, 
                     QueueDataType_t * data)
{
	if (queue_is_empty(queue))
	{
		return false;
	}
	
	*data = queue->data[queue->front_idx];
	if (queue->front_idx != queue->back_idx)
	{
		DECREASE_INDEX(queue, front_idx);
	}
	queue->elements--;
	return true;
}

/**
 * Reads one element from the back of the queue.
 * 
 * \returns false if the queue is empty.
*/
bool queue_get_back(struct queue * queue, 
                    QueueDataType_t * data)
{
	if (queue_is_empty(queue))
	{
		return false;
	}

	*data = queue->data[queue->back_idx];
	if (queue->front_idx != queue->back_idx)
	{
		INCREASE_INDEX(queue, back_idx);
	}
	queue->elements--;
	return true;
}
