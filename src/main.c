#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "clock_hal.h"
#include "uarte.h"
#include "nrf52.h"


char debug_buffer[100];

int main(void)
{
    hal_clock_init();
    uarte_init();
    while (true)
    {
        uarte_write_string("Hello world\r\n", 14);
        int x = 0xFFFF;
        while(x--);
    }
}
