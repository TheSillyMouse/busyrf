add_library(radio_drv STATIC)

target_sources(radio_drv
               radio_drv.c
               radio_drv.h)

target_include_directories(radio_drv .)