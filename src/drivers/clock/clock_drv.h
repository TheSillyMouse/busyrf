#ifndef CLOCK_DRV_H
#define CLOCK_DRV_H

#include <stdbool.h>

typedef enum
{
	CLK_SRC_RC,
	CLK_SRC_XTAL,
	CLK_SRC_SYNTH
}ClockSource_t;


void drv_clock_event_clear_hf_clk(void);

void drv_clock_event_clear_lf_clk(void);

bool drv_clock_event_get_hf_clk(void);

bool drv_clock_event_get_lf_clk(void);

void drv_clock_task_set_hf_clk(void);

void drv_clock_task_set_lf_clk(void);

void drv_clock_set_source(ClockSource_t clksrc);


#endif CLOCK_DRV_H