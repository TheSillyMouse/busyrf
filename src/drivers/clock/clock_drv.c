#include "nrf52.h"
#include "clock_drv.h"

void drv_clock_event_clear_hf_clk(void)
{
	NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
}

void drv_clock_event_clear_lf_clk(void)
{
	NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
}

bool drv_clock_event_get_hf_clk(void)
{
	return (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);
}

bool drv_clock_event_get_lf_clk(void)
{
	return (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);
}

void drv_clock_task_set_hf_clk(void)
{
	NRF_CLOCK->TASKS_HFCLKSTART = 1;
}

void drv_clock_task_set_lf_clk(void)
{
	NRF_CLOCK->TASKS_LFCLKSTART = 1;
}

void drv_clock_set_source(ClockSource_t clksrc)
{
	switch (clksrc) {
		case CLK_SRC_RC:
		{
			NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_RC << CLOCK_LFCLKSRC_SRC_Pos);
			break;
		}
		case CLK_SRC_XTAL:
		{
			NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos);
			break;
		}
		case CLK_SRC_SYNTH:
		{
			NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_Synth << CLOCK_LFCLKSRC_SRC_Pos);
			break;
		}
		default:
		{
			break;
		}
	}
}