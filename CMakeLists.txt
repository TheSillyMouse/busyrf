cmake_minimum_required(VERSION 3.18)

set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_SOURCE_DIR}/arm-none-eabi-gcc.cmake" CACHE PATH "toolchain file")

message(${CMAKE_TOOLCHAIN_FILE})

enable_language(C ASM)
set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

project(BusyRF)

set(EXECUTABLE ${PROJECT_NAME}.out)

add_compile_options(-O0 -mcpu=cortex-m4
                    -mthumb
                    -mabi=aapcs
                    -mfloat-abi=hard
                    -mfpu=fpv4-sp-d16
                    -ffunction-sections
                    -fdata-sections
                    -fno-strict-aliasing
                    -fno-builtin
                    -fshort-enums)
                    
add_link_options(-mthumb
                 -mabi=aapcs
                 -mcpu=cortex-m4
                 -T${CMAKE_CURRENT_SOURCE_DIR}/ldscripts/gcc_nrf52.ld
                 -mfloat-abi=hard
                 -mfpu=fpv4-sp-d16
                 LINKER:--gc-sections
                 --specs=nano.specs)

link_directories(ldscripts)



set(SRC_FILES src/main.c)

add_executable(${EXECUTABLE} ${SRC_FILES})

add_subdirectory(src/hal/clock     clock_hal)
add_subdirectory(src/drivers/clock clock_drv)
add_subdirectory(src/drivers/nrf52 nrf52)
add_subdirectory(src/drivers/cmsis cmsis)
add_subdirectory(src/drivers/uarte uarte)
add_subdirectory(src/drivers/gpio  gpio_drv)

target_link_libraries(${EXECUTABLE}
                      PRIVATE
                      clock_hal
                      clock_drv
                      nrf52
                      cmsis
                      uarte
                      gpio_drv
                      -lc
                      -lnosys
                      -lm)

# Print executable size
add_custom_command(TARGET 
                   ${EXECUTABLE}
                   POST_BUILD
                   COMMAND arm-none-eabi-size ${EXECUTABLE})

# Create hex file
add_custom_command(TARGET 
                   ${EXECUTABLE}
                   POST_BUILD
                   COMMAND arm-none-eabi-objcopy -O ihex ${EXECUTABLE} ${PROJECT_NAME}.hex
                   COMMAND arm-none-eabi-objcopy -O binary ${EXECUTABLE} ${PROJECT_NAME}.bin)
